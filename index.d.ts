export namespace ANTS {
    interface ApplicationConfiguration {
        pinAttempt: number,
        pinTimeToLive: string;
        verifyPinLimit: string;
        sendPinPerApplicationLimit: string;
        allowMultiplePinVerifications: boolean;
    }
    interface CreateTemplateParams {
        pinType: 'NUMERIC' | 'ALPHANUMERIC' | 'ALPHA' | 'HEX';
        pinPlaceholder?: string;
        messageText: string;
        pinLength?: number;
        language?: "en";
        repeatDTMF?: string;
        speechRate?: number;
        throttleAutoRetry?: boolean;
    }
    interface CreateTemplateResponse {
        messageId: string;
        applicationId: string;
        name: string;
        configuration: ANTS.ApplicationConfiguration,
        enabled: boolean;
        processId: string;
    }
    interface SendOTPResponse {
        pinId: string;
        to: string;
        ncStatus: string;
        smsStatus: string;
    }
}

export type sendOTP = (to: string, messageId: string) => Promise<string>
export type resendOTP = (pinId: string) => Promise<string>;
export type verifyOTP = (pinId: string, otp: string) => Promise<string>;
export interface SMS {
    sendOTP(to: string, messageId: string): string;
    verifyOTP(pinId: string, otp: string): boolean;
    resendOTP(pinId: string): Promise<void>;
}
export function SMSConstructor(options: {
    username: string;
    password: string;
    applicationId: string;
    senderId: string;
}): SMS
export default SMSConstructor;
