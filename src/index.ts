/// <reference path="../index.d.ts" /> 

import axios, { AxiosResponse } from 'axios';
import { ANTS } from '..';
export default (options: {
    username: string;
    password: string;
    applicationId: string;
    senderId: string;
}) => {
    const { username, password, applicationId, senderId } = options;
    // const authToken = Buffer.from(`${username}:${password}`).toString('base64');
    const api = axios.create({
        baseURL: 'https://api2.ants.co.th/', 
        auth: {
            username,
            password,
        },
        withCredentials: true,
        validateStatus: (status) => status < 500,
    });
    function handleResponse(result: AxiosResponse) {
        switch(result.status) {
            case 400:
                    throw new Error('INVLAID_ARGUMENT');
            case 404:
                throw new Error("APPLICATION_NOT_FOUND");
            case 429:
                if(!!process.env.NO_LOG) {
                    console.warn('[ANTS-SMS] Warning: Endpoint Throtthling')
                }
                // TODO ThrottleAutoRetry
                throw new Error('THROTTLE');
            default: 
                throw new Error('UNHANLDED_SMS_ERROR')
        }
    }
    return {
        async sendSMS() {

            throw Error('NOT_IMPLEMENT');
            /// TODO
        },
        async sendOTP(to: string, messageId: string) {
            const result = await api.post<ANTS.SendOTPResponse>('/2fa/1/pin', {
                applicationId,
                messageId,
                from: senderId,
                to,
            });
            console.info(result.data);

            switch(result.status) {
                case 200:
                    if (result.data.smsStatus === 'MESSAGE_NOT_SENT') {
                        throw new Error('MESSAGE_NOT_SENT');
                    } else {
                        return result.data.pinId;
                    }
                    // break;
                default:
                    handleResponse(result);
            }
        },
        async verifyOTP(pinId: string, otp: string): Promise<boolean> {
            // TODO TYPED
            const result = await api.post(`/2fa/1/pin/${pinId}/verify`, {
                pin: otp
            });
            console.log(result.data);
            return result.data.verified;

        },
        async resendOTP(pinId: string) {
            console.log('resending otp', pinId)
            await api.post(`/2fa/1/pin/${pinId}/resend`)  ;
        },
        async createTemplate({
            pinType = 'NUMERIC',
            language = 'en',
            messageText,
            pinLength = 4,
            pinPlaceholder = '<pin>',
            repeatDTMF = '1#',
            speechRate = 1,
            throttleAutoRetry = false
        }: ANTS.CreateTemplateParams) {
            const result = await api.post<ANTS.CreateTemplateResponse>(`/2fa/1/applications/${applicationId}/messages`, {
                pinType,
                language,
                messageText,
                pinLength,
                pinPlaceholder,
                repeatDTMF,
                speechRate,
            });
            switch (result.status) {
                case 200:
                    return result.data.messageId
                default:
                    handleResponse(result);
            }
        },

    }
}